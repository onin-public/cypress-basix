describe('happy path', () => {
  beforeEach(() => {
    cy.visit('http://sample.test/myheroes/createhero')
  });

  it('allows the user to submit a hero', () => {
    let name = 'Batman';

    cy.get('h1').should('contain', 'Create Hero');

    cy.get('input[name="name"]').type(name);
    cy.get('select[name="side"]').select('dark');
    cy.get('input[name="hitpoints"]').type('450');
    cy.get('input[name="attack"]').type('30');
    cy.get('input[name="special_ability"]').type('money!');
    cy.get('button').click();

    cy.get('p').should('contain.text', `Succesfully created hero ${name}!`);
  });
})