# Cypress Basix

This project demonstrates a basic automated UI test created using the awesome Cypress.js

## Requirements

1. Node.js version 16 or later

## Installation

`npm install`

## Test Execution

1. `npx cypress open`
2. Wait for the browser to open
3. Select and run the tests ("Specs") that you want

## Editing the tests

Just edit the file cypress/e2e/spec.cy.js, happy testing! :smile: :beers:
